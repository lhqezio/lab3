/**
* 
* @author Hoang Quoc Le 2131769
* Dawson College Fall 2022
*
*/
package linearalgebra;

public class Vector3d {
    private final double x;
    private final double y;
    private final double z; 
    public Vector3d(double x,double y,double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public double getX() {
        return x;
    }
    public double getY() {
        return y;
    }
    public double getZ() {
        return z;
    }
    public double magnitude() {
        double magnitude = Math.sqrt(Math.pow(x, 2)+Math.pow(y, 2)+Math.pow(z, 2));
        return magnitude;
    }
    public double dotProduct(Vector3d otherVector) {
        double result = this.x * otherVector.getX() + this.y * otherVector.getY() + this.z * otherVector.getZ();
        return result;
    }
    public Vector3d add(Vector3d otherVector) {
        Vector3d newVector3d = new Vector3d(this.x + otherVector.getX(), this.y + otherVector.getY(), this.z + otherVector.getZ());
        return newVector3d;
    }
}
