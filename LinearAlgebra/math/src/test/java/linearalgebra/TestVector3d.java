package linearalgebra;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestVector3d {
    @Test
    public void valueTest() {
        Vector3d tVector3d = new Vector3d(1, 1, 2);
        assertEquals(1, tVector3d.getX(),0);
        assertEquals(1, tVector3d.getY(),0);
        assertEquals(2, tVector3d.getZ(),0);
    }
    @Test
    public void magnitudeTest() {
        Vector3d tVector3d = new Vector3d(1, 1, 2);
        assertEquals(Math.sqrt(6), tVector3d.magnitude(),0);
        //Intentional Wrong Result
    }
    @Test
    public void dotProductTest() {
        Vector3d firstVector3d = new Vector3d(1, 1, 2);
        Vector3d secondVector3d = new Vector3d(2, 3, 4);
        assertEquals(13, firstVector3d.dotProduct(secondVector3d),0);
    }
    @Test
    public void addTest() {
        Vector3d firstVector3d = new Vector3d(1, 1, 2);
        Vector3d secondVector3d = new Vector3d(2, 3, 4);
        Vector3d resultVector3d = firstVector3d.add(secondVector3d);
        assertEquals(3, resultVector3d.getX(),0);
        assertEquals(4, resultVector3d.getY(),0);
        assertEquals(6, resultVector3d.getZ(),0);
    }
}
